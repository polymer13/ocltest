#pragma once

template<class ExitFunction>
struct ScopeGuard {

  ExitFunction exit;
  ScopeGuard(ExitFunction&& e): exit(e) {}
  ~ScopeGuard() { exit(); }

};

