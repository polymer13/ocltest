#include "file.hxx"
#include <cstdio>
#include <memory>
#include <sys/stat.h>

std::string read_file_to_string(const char *filename) {

  struct stat st;
  std::string str;

  int fine = stat(filename, &st);

  if (fine == -1) {
    printf("Could not stat file: %s\n", filename);
    return "";
  }

  std::unique_ptr<std::FILE, decltype(&fclose)> file(std::fopen(filename, "r"),
                                                     &fclose);

  if (!file) {
    printf("Could not open file for reading: %s\n", filename);
    return "";
  }

  str.resize(st.st_size);

  size_t count = fread(&str[0], 1, str.size(), file.get());

  if (count != static_cast<size_t>(st.st_size)) {
    printf("Could not read file entirely: %s\n", filename);
    return "";
  }

  return str;
}
