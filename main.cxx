#define CL_TARGET_OPENCL_VERSION 200
#include "file.hxx"
#include "scope_guard.hxx"
#include <CL/cl.h>
#include <cassert>
#include <iostream>
#include <vector>

struct OpenCLDevice {
  cl_device_id id;
  std::string name;
  cl_bool available;
  cl_ulong mem_size;
  cl_uint mem_cache_size;
};

struct OpenCLPlatform {
  cl_platform_id id;
  std::vector<OpenCLDevice> devices;
};

std::vector<OpenCLPlatform> ocl_platforms;

void ocl_error_callback(const char *err_info, const void *private_info,
                        size_t cb, void *user_data) {
  printf("OpenCL error occured:\n %s", err_info);
  exit(1);
}

void ocl_program_build_fail_callback(cl_program prog, void *user_data) {
  printf("Failed to build OpenCL program!\n");
  exit(1);
}

cl_bool ocl_is_device_available(cl_device_id id) {

  cl_bool available;

  cl_int err = clGetDeviceInfo(id, CL_DEVICE_AVAILABLE, sizeof(available),
                               &available, nullptr);

  if (err != CL_SUCCESS) {
    printf("Failed to determine if device is available (dev: %p, err: %d)!\n",
           id, err);
    return false;
  }

  return available;
}

std::string ocl_get_device_name(cl_device_id id) {

  char name_buf[1024];

  cl_int err =
      clGetDeviceInfo(id, CL_DEVICE_NAME, sizeof(name_buf), &name_buf, nullptr);

  if (err != CL_SUCCESS) {
    printf("Failed to get OpenCL device name!");
    return "unnamed";
  }

  return name_buf;
}

cl_ulong ocl_get_device_mem_size(cl_device_id id) {

  cl_ulong mem_size;

  cl_int err = clGetDeviceInfo(id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(mem_size),
                               &mem_size, nullptr);

  if (err != CL_SUCCESS) {
    printf("Failed to get OpenCL device mem size!\n");
    return 0;
  }

  return mem_size;
}

cl_uint ocl_get_device_mem_cache_size(cl_device_id id) {

  cl_ulong mem_cache_size;

  cl_int err =
      clGetDeviceInfo(id, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
                      sizeof(mem_cache_size), &mem_cache_size, nullptr);

  if (err != CL_SUCCESS) {
    printf("Failed to get OpenCL device mem cache size!\n");
    return 0;
  }

  return mem_cache_size;
}

void prepare_ocl_devices() {

  std::vector<cl_device_id> device_ids;

  for (auto &platform : ocl_platforms) {
    device_ids.resize(0);
    platform.devices.resize(0);

    cl_uint device_count_in_platform = 0;
    clGetDeviceIDs(platform.id, CL_DEVICE_TYPE_ALL, 0, nullptr,
                   &device_count_in_platform);

    device_ids.resize(device_count_in_platform);
    platform.devices.reserve(device_count_in_platform);

    cl_int err =
        clGetDeviceIDs(platform.id, CL_DEVICE_TYPE_ALL,
                       device_count_in_platform, &device_ids[0], nullptr);

    if (err != CL_SUCCESS) {
      printf("Failed to get OpenCL devices for platform: %p\n", platform.id);
      exit(1);
    }

    for (cl_uint i = 0; i < device_ids.size(); i++) {
      cl_device_id dev_id = device_ids[i];
      platform.devices.push_back({dev_id, ocl_get_device_name(dev_id),
                                  ocl_is_device_available(dev_id),
                                  ocl_get_device_mem_size(dev_id),
                                  ocl_get_device_mem_cache_size(dev_id)});
    }
  }
}

static cl_uint ocl_number_of_platforms() {
  cl_uint platform_count = 0;
  cl_int err = clGetPlatformIDs(0, nullptr, &platform_count);

  if (err != CL_SUCCESS) {
    printf("Failed to get number of OpenCL platforms (error_code: %d)\n", err);
  }

  return platform_count;
}

static void prepare_ocl_platforms() {

  std::vector<cl_platform_id> platform_ids;
  ocl_platforms.resize(0);

  cl_uint platform_count = ocl_number_of_platforms();

  platform_ids.resize(platform_count);

  printf("OpenCL platform count: %d\n", platform_count);

  cl_int err = clGetPlatformIDs(platform_count, &platform_ids[0], nullptr);

  if (err != CL_SUCCESS) {
    fprintf(stderr, "Failed to get OpenCL platform IDs\n");
    exit(1);
  }

  ocl_platforms.reserve(platform_count);
  for (auto id : platform_ids) {
    ocl_platforms.push_back({id});
  }
}

int main(int argc, char **argv) {

  prepare_ocl_platforms();
  prepare_ocl_devices();

  for (auto &platform : ocl_platforms) {
    printf("Platform device count: %lu\n", platform.devices.size());
    for (auto &dev : platform.devices) {
      std::cout << "OpenCL device: "
                << "\n\t name: " << dev.name
                << "\n\t available: " << dev.available
                << "\n\t memory size: " << dev.mem_size / 1000000 << "MB"
                << "\n\t memory cache size: " << dev.mem_cache_size / 1000
                << "KB" << std::endl;
    }
  }

  // For now use the first device in the first platform

  assert(ocl_platforms.size() > 0 && ocl_platforms[0].devices.size() > 0);

  const cl_platform_id used_platform = ocl_platforms[0].id;
  const cl_device_id used_device = ocl_platforms[0].devices[0].id;

  const cl_context_properties ocl_props[] = {
      CL_CONTEXT_PLATFORM, (cl_context_properties)used_platform, 0, 0};

  cl_int err;

  cl_context ocl_ctx = clCreateContext(ocl_props, 1, &used_device,
                                       ocl_error_callback, used_device, &err);

  if (err != CL_SUCCESS) {
    printf("Failed to create OpenCL context!\n");
    exit(1);
  }

  const cl_command_queue_properties root_queue_props[] = {
      CL_QUEUE_PROPERTIES, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, 0, 0};

  cl_command_queue root_queue = clCreateCommandQueueWithProperties(
      ocl_ctx, used_device, root_queue_props, &err);

  if (err != CL_SUCCESS) {
    printf("Failed to create a command queue for OpenCL context! (%d)\n", err);
    clReleaseContext(ocl_ctx);
    exit(1);
  }

  ScopeGuard ocl_cleanup = [&] {
    printf("Cleaning up OpenCL\n");
    clReleaseCommandQueue(root_queue);
    clReleaseContext(ocl_ctx);
  };

  float numbers[] = {0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
                     11, 12, 13, 14, 15, 16, 17, 18, 19, 20};

  cl_mem num_buffer = clCreateBuffer(ocl_ctx, CL_MEM_READ_WRITE,
                                     sizeof(numbers), nullptr, &err);

  if (err != CL_SUCCESS) {
    printf("Failed to create OpenCL buffer object! (%d)\n", err);
    exit(1);
  }

  err = clEnqueueWriteBuffer(root_queue, num_buffer, CL_TRUE, 0,
                             sizeof(numbers), &numbers, 0, nullptr, nullptr);

  if (err != CL_SUCCESS) {
    printf("Could not enqueue write to OpenCL buffer!(err: %d)\n", err);
  }

  std::string kernel_src = read_file_to_string("../test.cl");

  std::cout << "Kernel source: \n" << kernel_src << std::endl;

  const char *src_strings[] = {kernel_src.c_str(), 0, 0};
  const size_t src_lens[] = {kernel_src.size(), 0, 0};

  cl_program program = clCreateProgramWithSource(
      ocl_ctx, 1, static_cast<const char **>(src_strings), src_lens, &err);

  if (err != CL_SUCCESS) {
    printf("Failed to create OpenCL program from source (err: %d)\n", err);
    exit(1);
  }

  ScopeGuard program_cleanup = [&] {
    printf("Releasing OpenCL program!\n");
    clReleaseProgram(program);
  };

  err = clBuildProgram(program, 1, &used_device, nullptr, nullptr, nullptr);

  if (err != CL_SUCCESS) {

    if (err == CL_BUILD_PROGRAM_FAILURE) {
      cl_build_status build_status;
      cl_ulong log_size;
      clGetProgramBuildInfo(program, used_device, CL_PROGRAM_BUILD_STATUS,
                            sizeof(cl_build_status), &build_status, nullptr);

      if (build_status == CL_BUILD_ERROR) {
        std::string log;
        clGetProgramBuildInfo(program, used_device, CL_PROGRAM_BUILD_LOG, 0,
                              nullptr, &log_size);
        log.resize(log_size);
        err = clGetProgramBuildInfo(program, used_device, CL_PROGRAM_BUILD_LOG,
                                    log_size, &log[0], nullptr);
        if (err != CL_SUCCESS) {
          printf("Failed to get OpenGL build error log (err: %d)\n", err);
          exit(1);
        }

        std::cout << log << std::endl;
      }
    }
    printf("Failed to build OpenCL program\n (err: %d)\n", err);
    exit(1);
  }

  cl_kernel hello_kern = clCreateKernel(program, "hello", &err);

  if (err != CL_SUCCESS) {
    printf("Could not create OpenCL kernel (err: %d)\n");
    exit(1);
  }

  ScopeGuard hello_kern_cleanup = [&] {
    printf("Releasing hello kernel!\n");
    clReleaseKernel(hello_kern);
  };

  // clSetKernelArg(cl_kernel kernel, cl_uint arg_index, size_t arg_size, const
  // void *arg_value); clEnqueueNDRangeKernel(cl_command_queue command_queue,
  // cl_kernel kernel, cl_uint work_dim, const size_t *global_work_offset, const
  // size_t *global_work_size, const size_t *local_work_size, cl_uint
  // num_events_in_wait_list, const cl_event *event_wait_list, cl_event *event)

  return 0;
}
